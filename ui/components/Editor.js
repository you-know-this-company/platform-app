import React from 'react';
import { Editor } from 'react-draft-wysiwyg';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const toolbarStyle = {
    border: '1px solid rgba(0, 0, 0, 0.23)',
    borderRadius: '4px'
};

const editorStyle = {
    border: '1px solid rgba(0, 0, 0, 0.23)',
    borderRadius: '4px',
    padding: '18.5px 14px',
    minHeight: 300
};

const EditorComponent = props => (
    <Editor toolbarStyle={toolbarStyle} editorStyle={editorStyle} {...props} />
);

export default EditorComponent;

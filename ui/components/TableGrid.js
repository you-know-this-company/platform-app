import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTable } from 'react-table';

import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TablePagination
} from '@material-ui/core';

const TableGrid = ({ columns, data, pagination, onChange }) => {
    const instance = useTable({ columns, data });

    const { getTableProps, headerGroups, rows, prepareRow } = instance;

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const rowsPerPageOptions = [5, 10, 25];

    const count = (pagination && pagination.totalElements) || rows.length;

    const handleChangePage = (event, page) => {
        setPage(page);

        if (onChange) {
            onChange({
                page: page + 1
            });
        }
    };

    const handleChangeRowsPerPage = event => {
        const { value } = event.target;

        setPage(1);
        setRowsPerPage(value);

        if (onChange) {
            onChange({
                page: 1,
                pageSize: value
            });
        }
    };

    return (
        <>
            <Table {...getTableProps()}>
                <TableHead>
                    {headerGroups.map(headerGroup => (
                        <TableRow {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <TableCell {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableHead>
                <TableBody>
                    {rows.map(row => {
                        prepareRow(row);
                        return (
                            <TableRow {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return (
                                        <TableCell {...cell.getCellProps()}>
                                            {cell.render('Cell')}
                                        </TableCell>
                                    );
                                })}
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
            {pagination && (
                <TablePagination
                    component="div"
                    count={count}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                    page={page}
                    rowsPerPage={rowsPerPage}
                    rowsPerPageOptions={rowsPerPageOptions}
                />
            )}
        </>
    );
};

TableGrid.propTypes = {
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            Header: PropTypes.string.isRequired,
            accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
                .isRequired
        })
    ),
    rows: PropTypes.array,
    data: PropTypes.array,
    pagination: PropTypes.shape({
        pageSize: PropTypes.number,
        totalPages: PropTypes.number,
        totalElements: PropTypes.number
    }),
    onChange: PropTypes.func
};

TableGrid.defaultProps = {
    columns: [],
    rows: [],
    data: [],
    pagination: null,
    onChange: () => {}
};

export default TableGrid;

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';

import TableGrid from './TableGrid';
import { requests } from '../utils';

const { getData } = requests;

const Listings = ({ type, columns, actions }) => {
    const [rows, setRows] = useState([]);

    const [pagination, setPagination] = useState({
        page: 1,
        pageSize: 10,
        totalPages: null,
        totalElements: null
    });

    useEffect(() => {
        getData(type, pagination)
            .then(data => {
                const { docs, pagination } = data;

                setRows(docs);
                setPagination(pagination);
            })
            .catch(console.error);
    }, [pagination.page, pagination.pageSize]);

    const handleTableChange = props => {
        setPagination({
            ...pagination,
            ...props
        });
    };

    return (
        <Grid container spacing={3}>
            {actions.length > 0 && (
                <Grid container spacing={3} justify="flex-end">
                    {actions.map((action, idx) => (
                        // eslint-disable-next-line react/no-array-index-key
                        <Grid key={idx} item>
                            {action}
                        </Grid>
                    ))}
                </Grid>
            )}

            <Grid item xs={12}>
                <TableGrid
                    columns={columns}
                    data={rows}
                    pagination={pagination}
                    onChange={handleTableChange}
                />
            </Grid>
        </Grid>
    );
};

Listings.propTypes = {
    type: PropTypes.string.isRequired,
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            Header: PropTypes.string.isRequired,
            accessor: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
                .isRequired
        })
    ),
    actions: PropTypes.arrayOf(PropTypes.node)
};

Listings.defaultProps = {
    columns: [],
    actions: []
};

export default Listings;

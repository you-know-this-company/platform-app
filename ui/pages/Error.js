import React from 'react';
import { Link } from 'react-router-dom';

import { Layout } from '../components';

const Error = () => (
    <Layout title="Error">
        <h1>Error</h1>
        <p>Something went wrong!</p>

        <Link to="/">Home Page</Link>
    </Layout>
);

export default Error;

import React from 'react';

import { Layout, Listings } from '../components';
import { parsers } from '../utils';

const { parseDate, parseObject, parsePriority } = parsers;

const columns = [
    { Header: 'Name', accessor: 'name' },
    { Header: 'Priority', accessor: row => parsePriority(row.priority) },
    {
        Header: 'Details',
        accessor: row => (
            <span
                style={{
                    whiteSpace: 'pre'
                }}
            >
                {parseObject(row.data)}
            </span>
        )
    },
    {
        Header: 'Scheduled For',
        accessor: row => parseDate(row.nextRunAt)
    },
    {
        Header: 'Fail Reason',
        accessor: 'failReason'
    },
    {
        Header: 'Mailgun ID',
        accessor: row => row.mailgun_id || 'pending'
    }
];

const Tasks = () => {
    return (
        <Layout title="Jobs">
            <Listings type="jobs" columns={columns} />
        </Layout>
    );
};

export default Tasks;

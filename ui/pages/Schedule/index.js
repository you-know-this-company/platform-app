import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { RRule } from 'rrule';
import { useHistory } from 'react-router-dom';
import validate from 'validate.js';
import { convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import sanitizeHtml from 'sanitize-html';

import { Box, Button, Grid, Paper, TextField } from '@material-ui/core';
import { DateTimePicker } from '@material-ui/pickers';
import MuiAlert from '@material-ui/lab/Alert';

import { Layout, Editor } from '../../components';
import { requests } from '../../utils';

import After from './After';
import RepeatEvery from './RepeatEvery';
import Time from './Time';
import Weekdays from './Weekdays';
import End from './End';

const { postEmail } = requests;

const schema = {
    to: {
        presence: { allowEmpty: false, message: 'is required' },
        email: true
    },
    subject: {
        presence: { allowEmpty: false, message: 'is required' }
    },
    content: {
        presence: { allowEmpty: false, message: 'is required' }
    }
};

const Schedule = () => {
    const history = useHistory();

    const [state, setState] = useState({
        formState: {
            isValid: false,
            values: {},
            touched: {},
            errors: {}
        },
        visible: {
            once: false,
            rule: false,
            after: false,
            onDate: false,
            weekdays: false
        },
        options: {},
        timer: 'now'
    });

    const { formState, visible, options } = state;

    useEffect(() => {
        const errors = validate(formState.values, schema);

        setState(prevState => ({
            ...prevState,
            formState: {
                ...prevState.formState,
                isValid: !errors,
                errors: errors || {}
            }
        }));
    }, [formState.values]);

    const handleChange = event => {
        event.persist();

        const { name, value } = event.target;

        setState(prevState => ({
            ...prevState,
            formState: {
                ...prevState.formState,
                values: {
                    ...prevState.formState.values,
                    [name]: value
                },
                touched: {
                    ...prevState.formState.touched,
                    [name]: true
                }
            }
        }));
    };

    const handleEditorChange = editorState => {
        const raw = convertToRaw(editorState.getCurrentContent());

        // TODO: When converting Draft.js to HTML directly, some dangerous html gets encoded and cannot be sanitized correctly. As a workaround, we sanitize before the convertion to html
        raw.blocks.forEach(block => (block.text = sanitizeHtml(block.text)));

        const dirtyHtml = draftToHtml(raw);
        const cleanHtml = sanitizeHtml(dirtyHtml);

        setState(prevState => ({
            ...prevState,
            formState: {
                ...prevState.formState,
                values: {
                    ...prevState.formState.values,
                    content: cleanHtml
                },
                touched: {
                    ...prevState.formState.touched,
                    content: true
                }
            }
        }));
    };

    const handleChangeRuleValue = ev => {
        const { name, value } = ev.target;

        setState(prevState => ({
            ...prevState,
            options: {
                ...prevState.options,
                [name]: value
            }
        }));
    };

    const handleChangeFrequency = ev => {
        const { name, value } = ev.target;

        const newVisible = {
            weekdays: false
        };

        switch (value) {
            case 'WEEKLY':
                newVisible.weekdays = true;
                break;

            default:
                break;
        }

        // Reset frequency
        const { byweekday, freq, ...rest } = options;

        setState(prevState => ({
            ...prevState,
            visible: {
                ...prevState.visible,
                ...newVisible
            },
            options: {
                ...rest,
                [name]: RRule[value]
            }
        }));
    };

    const handleChangeWeekdays = days => {
        const chechedDays = Object.keys(days).filter(key => days[key]);
        const byweekday = chechedDays.map(d => RRule[d]);

        let newOptions = {};

        // TODO: Not updated immediately
        if (byweekday.length) {
            newOptions.byweekday = byweekday;
        } else {
            // Reset weekdays
            const { byweekday, ...rest } = options;
            newOptions = rest;
        }

        setState(prevState => ({
            ...prevState,
            options: {
                ...prevState.options,
                ...newOptions
            }
        }));
    };

    const handleChangeEnd = ev => {
        const { value } = ev.target;

        const newVisible = {
            after: false,
            onDate: false
        };

        switch (value) {
            case 'after':
                newVisible.after = true;
                newVisible.onDate = false;
                break;

            case 'on_date':
                newVisible.after = false;
                newVisible.onDate = true;
                break;

            default:
                break;
        }

        // Reset rule
        const { count, until, ...rest } = options;

        setState(prevState => ({
            ...prevState,
            visible: {
                ...prevState.visible,
                ...newVisible
            },
            options: {
                ...rest
            }
        }));
    };

    const hasError = field =>
        !!(formState.touched[field] && formState.errors[field]);

    const handleDateChangeStart = date => {
        setState(prevState => ({
            ...prevState,
            options: {
                ...prevState.options,
                dtstart: date,
                count: 1
            }
        }));
    };

    const handleDateChangeEnd = date => {
        setState(prevState => ({
            ...prevState,
            options: {
                ...prevState.options,
                until: date
            }
        }));
    };

    //
    const handleSubmit = async e => {
        e.preventDefault();

        const { timer } = state;

        if (timer === 'now') {
            formState.values.schedule = '';
        } else {
            formState.values.schedule = rule.toString();
        }

        // alert(JSON.stringify(formState.values, null, 2));

        try {
            await postEmail(formState.values);
            history.push('/');
        } catch (error) {
            alert(JSON.stringify(error));
        }
    };

    const handleChangeTimer = ev => {
        const { name, value } = ev.target;

        const newVisible = {
            once: false,
            rule: false
        };

        const newOptions = {};

        switch (value) {
            case 'once':
                newVisible.once = true;
                newVisible.rule = false;
                break;
            case 'rule':
                newOptions.freq = RRule.YEARLY;
                newOptions.interval = 1;

                newVisible.once = false;
                newVisible.rule = true;
                break;

            default:
                break;
        }

        setState(prevState => ({
            ...prevState,
            visible: {
                ...prevState.visible,
                ...newVisible
            },
            options: {
                ...prevState.options,
                ...newOptions
            },
            timer: value
        }));
    };

    const rule = new RRule(options);

    return (
        <Layout title="Schedule an email">
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <form onSubmit={handleSubmit} autoComplete="off">
                        <Paper style={{ padding: 16 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        label="To"
                                        variant="outlined"
                                        error={hasError('to')}
                                        fullWidth
                                        helperText={
                                            hasError('to')
                                                ? formState.errors.to[0]
                                                : null
                                        }
                                        name="to"
                                        onChange={handleChange}
                                        value={formState.values.to || ''}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        label="Subject"
                                        variant="outlined"
                                        error={hasError('subject')}
                                        fullWidth
                                        helperText={
                                            hasError('subject')
                                                ? formState.errors.subject[0]
                                                : null
                                        }
                                        name="subject"
                                        onChange={handleChange}
                                        value={formState.values.subject || ''}
                                    />
                                </Grid>

                                <Grid item xs={12} />
                                <Grid item xs={12} />

                                <Grid item xs={12}>
                                    <Editor
                                        name="content"
                                        onEditorStateChange={handleEditorChange}
                                    />
                                </Grid>

                                <Grid item xs={12}>
                                    <Time handleChange={handleChangeTimer} />
                                </Grid>

                                {visible.once && (
                                    <Grid item xs={12}>
                                        <DateTimePicker
                                            inputVariant="outlined"
                                            ampm={false}
                                            disablePast
                                            // value={selectedDate}
                                            onChange={handleDateChangeStart}
                                        />
                                    </Grid>
                                )}

                                {visible.rule && (
                                    <>
                                        <Grid item xs={12}>
                                            <div>
                                                <RepeatEvery
                                                    handleChangeRuleValue={
                                                        handleChangeRuleValue
                                                    }
                                                    handleChangeFrequency={
                                                        handleChangeFrequency
                                                    }
                                                    options={options}
                                                />

                                                <br />

                                                {visible.weekdays && (
                                                    <Weekdays
                                                        handleChange={
                                                            handleChangeWeekdays
                                                        }
                                                    />
                                                )}

                                                <br />
                                                <br />

                                                <div>
                                                    <Grid
                                                        container
                                                        spacing={2}
                                                        alignItems="center"
                                                    >
                                                        <Grid item>
                                                            <Box
                                                                fontWeight="fontWeightBold"
                                                                m={1}
                                                            >
                                                                End
                                                            </Box>
                                                        </Grid>

                                                        <Grid item>
                                                            <End
                                                                handleChange={
                                                                    handleChangeEnd
                                                                }
                                                            />
                                                        </Grid>

                                                        {visible.after && (
                                                            <After
                                                                handleChange={
                                                                    handleChangeRuleValue
                                                                }
                                                            />
                                                        )}

                                                        {visible.onDate && (
                                                            <Grid item>
                                                                <DateTimePicker
                                                                    inputVariant="outlined"
                                                                    ampm={false}
                                                                    disablePast
                                                                    // value={
                                                                    //     selectedDate
                                                                    // }
                                                                    onChange={
                                                                        handleDateChangeEnd
                                                                    }
                                                                />
                                                            </Grid>
                                                        )}
                                                    </Grid>
                                                </div>
                                            </div>
                                        </Grid>
                                        <Grid item xs={6}>
                                            <MuiAlert
                                                elevation={4}
                                                variant="filled"
                                                severity="success"
                                            >
                                                {rule.toText()}
                                            </MuiAlert>
                                        </Grid>
                                    </>
                                )}

                                <Grid item xs={12}>
                                    <Button
                                        type="submit"
                                        color="primary"
                                        disabled={!formState.isValid}
                                        size="large"
                                        variant="contained"
                                    >
                                        Create
                                    </Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </form>
                </Grid>
            </Grid>
        </Layout>
    );
};

export default Schedule;

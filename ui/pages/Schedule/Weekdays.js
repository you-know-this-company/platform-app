import React, { useState } from 'react';
import PropTypes from 'prop-types';

import {
    Box,
    Checkbox,
    Grid,
    FormControl,
    FormControlLabel,
    FormGroup
} from '@material-ui/core';

const days = [
    { value: 'MO', label: 'Monday' },
    { value: 'TU', label: 'Tuesday' },
    { value: 'WE', label: 'Wednesday' },
    { value: 'TH', label: 'Thursday' },
    { value: 'FR', label: 'Friday' },
    { value: 'SA', label: 'Saturday' },
    { value: 'SU', label: 'Sunday' }
];

const Weekdays = ({ handleChange }) => {
    const [state, setState] = useState({
        MO: false,
        TU: false,
        WE: false,
        TH: false,
        FR: false,
        SA: false,
        SU: false
    });

    return (
        <Grid container spacing={2} alignItems="center">
            <Grid item>
                <Box fontWeight="fontWeightBold" m={1}>
                    Repeat on
                </Box>
            </Grid>
            <Grid item>
                <FormControl component="fieldset">
                    <FormGroup row>
                        {days.map(day => {
                            const { value, label } = day;

                            return (
                                <FormControlLabel
                                    key={day.value}
                                    control={
                                        <Checkbox
                                            name={value}
                                            checked={state[value]}
                                            onChange={() => {
                                                const newState = {
                                                    ...state,
                                                    [value]: !state[value]
                                                };

                                                setState(newState);

                                                handleChange(newState);
                                            }}
                                            color="primary"
                                        />
                                    }
                                    label={label}
                                />
                            );
                        })}
                    </FormGroup>
                </FormControl>
            </Grid>
        </Grid>
    );
};

Weekdays.propTypes = {
    handleChange: PropTypes.func.isRequired
};

export default Weekdays;

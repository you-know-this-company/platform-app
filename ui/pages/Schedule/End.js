import React from 'react';
import PropTypes from 'prop-types';

import { FormControl, Select, MenuItem } from '@material-ui/core';

const End = ({ handleChange }) => {
    return (
        <FormControl variant="outlined">
            <Select name="end" onChange={handleChange} defaultValue="never">
                <MenuItem value="never">Never</MenuItem>
                <MenuItem value="after">After</MenuItem>
                <MenuItem value="on_date">On date</MenuItem>
            </Select>
        </FormControl>
    );
};

End.propTypes = {
    handleChange: PropTypes.func.isRequired
};

export default End;

import React from 'react';
import PropTypes from 'prop-types';

import { FormControl, Select, MenuItem } from '@material-ui/core';

const style = {
    minWidth: 200
};

const Time = ({ handleChange }) => {
    return (
        <FormControl variant="outlined">
            <Select onChange={handleChange} defaultValue="now" style={style}>
                <MenuItem value="now">Now</MenuItem>
                <MenuItem value="once">Once</MenuItem>
                <MenuItem value="rule">Recurrently</MenuItem>
            </Select>
        </FormControl>
    );
};

Time.propTypes = {
    handleChange: PropTypes.func.isRequired
};

export default Time;

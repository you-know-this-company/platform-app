import React from 'react';
import PropTypes from 'prop-types';

import { Box, Grid, TextField } from '@material-ui/core';

const textFieldStyle = {
    width: 80
};

const After = ({ handleChange }) => {
    return (
        <Grid item>
            <Grid container spacing={2} alignItems="center">
                <Grid item>
                    <TextField
                        variant="outlined"
                        name="count"
                        onChange={handleChange}
                        style={textFieldStyle}
                    />
                </Grid>
                <Grid item>
                    <Box fontWeight="fontWeightBold" m={1}>
                        occurrence(s)
                    </Box>
                </Grid>
            </Grid>
        </Grid>
    );
};

After.propTypes = {
    handleChange: PropTypes.func.isRequired
};

export default After;

import React from 'react';
import PropTypes from 'prop-types';

import {
    Box,
    FormControl,
    Grid,
    MenuItem,
    Select,
    TextField
} from '@material-ui/core';

const textFieldStyle = {
    width: 80
};

const RepeatEvery = ({
    handleChangeRuleValue,
    handleChangeFrequency,
    options
}) => {
    return (
        <Grid container spacing={2} alignItems="center">
            <Grid item>
                <Box fontWeight="fontWeightBold" m={1}>
                    Repeat every
                </Box>
            </Grid>
            <Grid item>
                <TextField
                    variant="outlined"
                    name="interval"
                    onChange={handleChangeRuleValue}
                    style={textFieldStyle}
                    defaultValue={options.interval}
                />
            </Grid>
            <Grid item>
                <FormControl variant="outlined">
                    <Select
                        name="freq"
                        onChange={handleChangeFrequency}
                        defaultValue="YEARLY"
                    >
                        <MenuItem value="DAILY">Day</MenuItem>
                        <MenuItem value="WEEKLY">Week</MenuItem>
                        <MenuItem value="MONTHLY">Month</MenuItem>
                        <MenuItem value="YEARLY">Year</MenuItem>
                    </Select>
                </FormControl>
            </Grid>
        </Grid>
    );
};

RepeatEvery.propTypes = {
    handleChangeRuleValue: PropTypes.func.isRequired,
    handleChangeFrequency: PropTypes.func.isRequired
};

export default RepeatEvery;

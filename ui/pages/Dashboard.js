import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from '@material-ui/core';

import ScheduleIcon from '@material-ui/icons/Schedule';
import { Layout, Listings } from '../components';
import { parsers } from '../utils';

const { parseRRule, parseDate } = parsers;

const columns = [
    { Header: 'To', accessor: 'to' },
    { Header: 'Subject', accessor: 'subject' },
    { Header: 'Content', accessor: 'content' },
    { Header: 'Schedule', accessor: row => parseRRule(row.schedule) },
    { Header: 'Created At', accessor: row => parseDate(row.createdAt) }
];

const Dashboard = () => {
    const history = useHistory();

    const actions = [
        <Button
            variant="contained"
            color="primary"
            startIcon={<ScheduleIcon />}
            onClick={() => history.push('/schedule')}
        >
            New Mail
        </Button>
    ];

    return (
        <Layout title="Emails">
            <Listings type="emails" columns={columns} actions={actions} />
        </Layout>
    );
};

export default Dashboard;

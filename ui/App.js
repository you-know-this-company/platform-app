import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

import { Dashboard, Schedule, Tasks, Error } from './pages';

const App = () => (
    <>
        <CssBaseline />

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={Dashboard} exact />
                    <Route path="/schedule" component={Schedule} exact />
                    <Route path="/tasks" component={Tasks} exact />
                    <Route component={Error} />
                </Switch>
            </BrowserRouter>
        </MuiPickersUtilsProvider>
    </>
);

export default App;

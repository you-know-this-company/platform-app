const path = require('path');

module.exports = options => ({
    mode: options.mode,
    entry: path.resolve('ui', 'index.js'),
    output: {
        path: path.resolve(__dirname, '../build'),
        filename: 'bundle.js',
        publicPath: '/',
        ...options.output
    },
    optimization: options.optimization,
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: []
                    }
                },
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader']
            },
            {
                test: /\.html$/,
                use: {
                    loader: 'html-loader'
                }
            }
        ]
    },
    plugins: options.plugins.concat([]),
    devtool: options.devtool,
    performance: options.performance || {},
    devServer: options.devServer || {}
});

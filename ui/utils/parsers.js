import { RRule } from 'rrule';
import { format } from 'date-fns';

const NO_VALUE = 'N/A';
const FRIENDLY_DATE = 'yyyy-MM-dd HH:mm:ss';

const parseDate = date => {
    if (!date) return NO_VALUE;

    const parsed = format(new Date(date), FRIENDLY_DATE);

    return parsed;
};

const parseObject = obj => {
    if (!obj) return NO_VALUE;

    const parsed = JSON.stringify(obj, null, 2);

    return parsed;
};

const parseRRule = str => {
    if (!str) return NO_VALUE;

    const rule = RRule.fromString(str);

    const parsed = rule.toText();

    return parsed;
};

const parsePriority = priority => {
    switch (priority) {
        case 20:
            return 'Highest';
        case 10:
            return 'High';
        case 0:
            return 'Normal';
        case -10:
            return 'Low';
        case -20:
            return 'Lowest';
        default:
            return NO_VALUE;
    }
};

export default {
    parseDate,
    parseObject,
    parsePriority,
    parseRRule
};

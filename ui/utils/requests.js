const { BASE_URL } = process.env;

const headers = {
    'Content-Type': 'application/json'
};

export const getData = async (type, pagination) => {
    const { page, pageSize } = pagination;

    const query = `page=${page}&pageSize=${pageSize}`;

    const response = await fetch(`${BASE_URL}/${type}?${query}`);
    const data = await response.json();

    return data;
};

const postEmail = async email => {
    const body = JSON.stringify(email);

    const response = await fetch(`${BASE_URL}/emails`, {
        method: 'POST',
        headers,
        body
    });

    const data = await response.json();

    if (response.ok) {
        return data;
    }

    throw data;
};

export default {
    getData,
    postEmail
};

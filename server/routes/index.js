const router = require('express').Router();

const { getEmails, postEmail } = require('./email');
const { getJobs } = require('./job');

router
    .route('/emails')
    .get(getEmails)
    .post(postEmail);

router.route('/jobs').get(getJobs);

module.exports = router;

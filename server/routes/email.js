const { RRule } = require('rrule');

const { scheduler } = require('../utils');
const { Email } = require('../models');
const { constants } = require('../common');

const { SEND_EMAIL } = constants;

const getEmails = async (req, res, next) => {
    let { page, pageSize } = req.query;

    page = page ? parseInt(page, 10) : 1;
    pageSize = pageSize ? parseInt(pageSize, 10) : 10;

    try {
        const result = await Email.paginate(
            {},
            {
                page,
                pageSize,
                sort: {
                    createdAt: 'desc'
                }
            }
        );

        return res.json(result);
    } catch (error) {
        return next(error);
    }
};

const postEmail = async (req, res, next) => {
    try {
        const { subject, content, to, schedule = '' } = req.body;

        const email = new Email({
            subject,
            content,
            to,
            schedule
        });

        const doc = await email.save();

        if (schedule) {
            const rule = RRule.fromString(schedule);

            // TODO: No limit
            const limit = new Date(Date.UTC(2030, 11, 31, 0, 0, 0));
            const times = rule.between(new Date(), limit);

            await scheduler.schedule(Date.now(), SEND_EMAIL, {
                to,
                subject,
                content
            });

            const jobs = (times || []).map(time =>
                scheduler.schedule(time, SEND_EMAIL, { to, subject, content })
            );

            await Promise.all(jobs);
        } else {
            await scheduler.now(SEND_EMAIL, { to, subject, content });
        }

        return res.status(201).json(doc);
    } catch (error) {
        return next(error);
    }
};

module.exports = {
    getEmails,
    postEmail
};

const { Job } = require('../models');

const getJobs = async (req, res, next) => {
    let { page, pageSize } = req.query;

    page = page ? parseInt(page, 10) : 1;
    pageSize = pageSize ? parseInt(pageSize, 10) : 10;

    try {
        const result = await Job.paginate(
            {},
            {
                page,
                pageSize,
                sort: {
                    nextRunAt: 'desc'
                }
            }
        );

        return res.json(result);
    } catch (error) {
        return next(error);
    }
};

module.exports = {
    getJobs
};

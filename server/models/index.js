const Email = require('./email');
const Job = require('./job');

module.exports = {
    Email,
    Job
};

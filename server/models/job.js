const mongoose = require('mongoose');

const paginate = require('../plugins/paginate');

const jobSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    priority: {
        type: Number,
        required: true
    }
});

jobSchema.plugin(paginate);

module.exports = mongoose.model('job', jobSchema);

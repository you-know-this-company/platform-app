const mongoose = require('mongoose');

const paginate = require('../plugins/paginate');

const emailSchema = mongoose.Schema(
    {
        subject: {
            type: String,
            required: true
        },
        content: {
            type: String,
            required: true
        },
        to: [String],
        schedule: {
            type: String
        }
    },
    {
        timestamps: true
    }
);

emailSchema.plugin(paginate);

module.exports = mongoose.model('email', emailSchema);

const DEFAULT_PAGE = 1;
const DEFAULT_PAGE_SIZE = 10;

async function paginate(query = {}, options) {
    const {
        page = DEFAULT_PAGE,
        pageSize = DEFAULT_PAGE_SIZE,
        select,
        sort
    } = options;

    const skip = (page - 1) * pageSize;

    const docsQuery = this.find(query)
        .select(select)
        .sort(sort)
        .skip(skip)
        .limit(pageSize);

    const promises = [docsQuery.exec(), this.countDocuments(query).exec()];

    const [docs, totalElements] = await Promise.all(promises);

    const totalPages = Math.ceil(totalElements / pageSize) || 1;

    const result = {
        docs,
        pagination: {
            page,
            pageSize,
            totalPages,
            totalElements
        }
    };

    return result;
}

module.exports = schema => {
    // eslint-disable-next-line no-param-reassign
    schema.statics.paginate = paginate;
};

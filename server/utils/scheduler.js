const Agenda = require('agenda');

const { MONGODB_URI } = process.env;

const agenda = new Agenda({
    db: {
        address: MONGODB_URI,
        collection: 'jobs'
    }
});

module.exports = agenda;

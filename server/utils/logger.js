const fs = require('fs');
const path = require('path');
const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');

const { combine, timestamp, label, colorize, printf } = format;

const env = process.env.NODE_ENV || 'development';
const logDir = 'logs';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const consoleTransport = new transports.Console({
    format: combine(
        colorize(),
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    )
});

const friendlyLabel =
    env === 'test' ? 'test' : path.basename(process.mainModule.filename);

const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/%DATE%.log`,
    datePattern: 'YYYY-MM-DD',
    format: combine(
        timestamp(),
        label({ label: friendlyLabel }),
        printf(
            info =>
                `${info.timestamp} ${info.level} [${info.label}]: ${info.message}`
        )
    )
});

const logger = createLogger({
    level: env === 'production' ? 'info' : 'debug',
    transports: [consoleTransport, dailyRotateFileTransport],
    handleExceptions: true
});

module.exports = logger;

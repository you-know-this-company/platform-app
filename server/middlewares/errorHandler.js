const boom = require('boom');

const logger = require('../utils/logger');

const errorHandler = (error, req, res, next) => {
    logger.error(error.stack);

    if (!error.isBoom) {
        // eslint-disable-next-line no-param-reassign
        error = boom.boomify(error);
    }

    const { statusCode, payload } = error.output;

    return res.status(statusCode).json(payload);
};

module.exports = errorHandler;

const logger = require('../utils/logger');

const requestLogger = (req, res, next) => {
    const { url, method, query, body } = req;

    const request = JSON.stringify({
        url,
        method,
        query,
        body
    });

    logger.info(request);

    next();
};

module.exports = requestLogger;

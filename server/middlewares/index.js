const requestLoggger = require('./requestLogger');
const errorHandler = require('./errorHandler');

module.exports = {
    requestLoggger,
    errorHandler
};

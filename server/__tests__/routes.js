const supertest = require('supertest');

const { setupDB } = require('../../test-setup');
const { Email } = require('../models');
const app = require('../');

const request = supertest(app);

setupDB('testing', true);

const { API_URL } = process.env;

it('Fetch emails', async done => {
    const response = await request.get(`${API_URL}/emails`);

    expect(response.status).toBe(200);
    expect(response.body.docs).toBeTruthy();
    expect(response.body.pagination).toBeTruthy();

    done();
});

it('Schedule an email', async done => {
    const newEmail = {
        to: 'jon.snow@stark.com',
        subject: 'Test Subject',
        content: 'Test Content',
        schedule: 'FREQ=DAILY;INTERVAL=2;COUNT=4'
    };

    const res = await request.post(`${API_URL}/emails`).send(newEmail);

    // Ensures response contains name and email
    expect(res.body.subject).toBeTruthy();
    expect(res.body.content).toBeTruthy();
    expect(res.body.schedule).toBeTruthy();

    // Searches the email in the database
    const email = await Email.findOne({ to: 'jon.snow@stark.com' });

    expect(email.subject).toBeTruthy();
    expect(email.content).toBeTruthy();
    expect(email.schedule).toBeTruthy();

    done();
});

it('Fetch jobs', async done => {
    const response = await request.get(`${API_URL}/jobs`);

    expect(response.status).toBe(200);
    expect(response.body.docs).toBeTruthy();
    expect(response.body.pagination).toBeTruthy();

    done();
});

const notifications = {
    MONGODB_CONNECTION_SUCCESS: 'MongoDB connection establshed.',
    MONGODB_CONNECTION_ERROR: 'MongoDB connection failed.',
    SCHEDULER_STARTED: 'Scheduler started.',
    SERVER_LISTENING: 'Server is listening on localhost:'
};

const jobs = {
    SEND_EMAIL: 'SEND_EMAIL'
};

module.exports = {
    ...notifications,
    ...jobs
};

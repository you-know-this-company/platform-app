require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const { requestLoggger, errorHandler } = require('./middlewares');
const { logger, scheduler } = require('./utils');
const routes = require('./routes');
const { constants } = require('./common');

const {
    MONGODB_CONNECTION_SUCCESS,
    MONGODB_CONNECTION_ERROR,
    SCHEDULER_STARTED,
    SERVER_LISTENING
} = constants;

const {
    PORT = 3000,
    API_URL = '/api/v1',
    MONGODB_URI = 'mongodb://localhost:27017/send-the-mail'
} = process.env;

const server = express();

const mongooseOptions = { useNewUrlParser: true, useUnifiedTopology: true };

mongoose
    .connect(MONGODB_URI, mongooseOptions)
    .then(() => {
        logger.info(MONGODB_CONNECTION_SUCCESS);

        return scheduler.start();
    })
    .then(() => {
        logger.info(SCHEDULER_STARTED);

        server.use(cors());

        server.use(bodyParser.json());
        server.use(
            bodyParser.urlencoded({
                extended: true
            })
        );

        server.use(requestLoggger);

        // Handle all API v1 endpoints
        server.use(API_URL, routes);

        // Catch-all route
        server.get('*', (req, res) => {
            res.sendStatus(404);
        });

        server.use(errorHandler);

        server.listen(PORT, () => {
            logger.info(`${SERVER_LISTENING} ${PORT}`);
        });
    })
    .catch(err => {
        logger.error(MONGODB_CONNECTION_ERROR, err);
    });

module.exports = server;

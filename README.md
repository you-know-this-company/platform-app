# Platform App

A simple email scheduling platform

## Installation

```bash
npm install         # install necessary modules

npm run server      # run backend Express/Mongo server
npm run ui          # build and run front-end Webpack/React app

npm run test        # execute tests
```

If both SERVER and UI are running, then the application should be accessible from a browser at `http://localhost:8080`

An `.env-sample` file is provided for configuration (if needed). In a productiontion environment, we would copy it as an `.env` file and tweak desired settings.

Note, there are very, very few tests. The importance of detailed tests in the real-world is well understood. The tests here are **totally not meant** for "отбий номера".

## Technical decisions

### Back End

-   Recurring dates in the database are stored using the `RFC 5545 (iCalendar)` format. It is short, super flexible, and convenient e.g. `FREQ=WEEKLY;BYDAY=MO,WE`. The library `rrule` was picked to do the heavy lifting when working with iCalendar strings.
-   The big one is `agenda` - the library and general approach to handle jobs management and distribution. It is a very simple and straightforward way to achieve scheduling. Given the hard requirement to use MongoDB, `agenda` fits naturally as it uses MongoDB for data-persistence. In a real-world scenario, after discussing the expected requirements, I **would probably pick something more robust** like RabbitMQ or a Redis-based approach. Hope that's okay.

### Front End

-   Functional over Class components where possible
-   No big state management library (i.e. Redux) - just hooks and local state
-   `Materail UI` used for layout & styling
-   browser `fetch` used for making API request
-   React Router used for simple navigation
-   intentionally did not use a helper library to handle forms - would do in a real project
-   two build configurations available - for DEV and PROD - which share common Webpack settings

### General

-   Code styling is kept standardized using AirBnB eslint
-   Code formatting is kept standardized using Prettier

## Known issues & Improvements

-   the "Schedule" page should totally be refactored futher. Especially the bottom part of the page that form the "recurring options" should be its own component
-   More options to customize recurring emails
-   Pagination in Listings.js is not perfect at the moment - there is a warning related to setting the pagination twice - forgotten after I migrated from client-side to server-side pagination
-   Not exactly a problem here, but the logger middleware would log sensitive data (e.g. password)
-   work with a standard library for handling forms
-   a few no-unused-vars and 5-6 eslint errors remaining
-   no generic loading / notification indicator
-   in a serious project, I would use something like OpenAPI / Swagger to assist with API design and documentation, and possibly validation
-   not the biggest fan of TypeScript, but
